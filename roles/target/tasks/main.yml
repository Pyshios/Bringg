---

- name: Install packages
  apt:
    name:
      - unzip
      - acl
      - python3-pip
    update_cache: yes
    state: present

- name: Create monitoring network
  docker_network:
    name: monitoring
    state: present

- name: Ensure passlib is installed
  ansible.builtin.pip:
    name: passlib
    state: present
  become: yes

- name: Create folders
  file:
    state: directory
    name: "/srv/{{ item }}"
    recurse: yes
    mode: "a+rw"
  loop:
    - nginx
    - nginx/logs
    - promtail

- name: Copy systemd init file
  template:
    src: promtail/promtail.service.j2
    dest:  /etc/systemd/system/promtail.service

# Configure NGINX
- name: Create private key (RSA, 4096 bits)
  community.crypto.openssl_privatekey:
    path: "/srv/nginx/certificate.key"


- name: Create simple self-signed certificate
  community.crypto.x509_certificate:
    path: /srv/nginx/certificate.pem
    privatekey_path: /srv/nginx/certificate.key
    provider: selfsigned


- name: Add a user to a password file and ensure permissions are set
  community.general.htpasswd:
    path: /srv/nginx/.htpasswd
    name: "{{ htaccessusr }}"
    password: '{{ htaccesspwd }}'
    mode: a+r

- name: Deploy docker daemon config
  template:
    src: docker/daemon.json
    dest: /etc/docker/daemon.json
    owner: root
    group: root
    mode: '0644'

- name: Copy the agent nginx configuration
  template:
    src: nginx/nginx.conf.j2
    dest: /srv/nginx/nginx.conf



# Reverse proxy
- name: Deploy nginx
  docker_container:
    image: nginx:1.23.1-alpine
    name: nginx
    restart_policy: always
    networks:
      - name: monitoring
    restart: yes
    ports:
      - "80:80"
      - "443:443"
      - "3100:3100"
      - "9100:9100"
      - "8080:8080"
    mounts:
      type=bind
      source=/srv/nginx
      target=/etc/nginx


- name: Create NodeExporter
  docker_container:
    name: node-exporter
    restart_policy: always
    networks:
      - name: monitoring
    image: prom/node-exporter:{{ node_exporter_version }}
    volumes:
      - /proc:/host/proc:ro
      - /sys:/host/sys:ro
      - /:/rootfs:ro
    command: >
      --path.procfs=/host/proc
      --path.rootfs=/rootfs
      --path.sysfs=/host/sys
      --collector.filesystem.mount-points-exclude=^/(sys|proc|dev|host|etc)($$|/)

- name: Create cAdvisor
  docker_container:
    name: cadvisor
    networks:
      - name: monitoring
    restart_policy: always
    image: gcr.io/cadvisor/cadvisor:{{ cadvisor_version }}
    volumes:
      - /:/rootfs:ro
      - /var/run:/var/run:ro
      - /sys:/sys:ro
      - /var/lib/docker/:/var/lib/docker:ro
      - /dev/disk/:/dev/disk:ro

- name: Copy promtail config file
  template:
    src: 'promtail/config-promtail.yml.j2'
    dest: /srv/promtail/config-promtail.yml
    mode: '0644'  # Readable by others, not writable



- name: Download and unzip promtail
  ansible.builtin.unarchive:
    src: "https://github.com/grafana/loki/releases/download/v2.6.1/promtail-linux-{{ 'arm64' if ansible_architecture == 'aarch64' else 'amd64' }}.zip"
    dest: /var/tmp/
    remote_src: yes

- name: Copy files to the install locations
  copy:
    src: "/var/tmp/promtail-linux-{{ 'arm64' if ansible_architecture == 'aarch64' else 'amd64' }}"
    dest: /usr/local/bin
    remote_src: yes
    mode: a+x

- name: Change Log file permissions
  command:
    cmd: "setfacl -R -m u:ubuntu:rwX {{ item }}"
  loop:
    - "/var/log"
    - "/var/lib/docker/containers"
    - "/tmp"
  become: true

- name: Copy systemd init file
  template:
    src: promtail/promtail.service.j2
    dest: /etc/systemd/system/promtail.service




- name: Cleaning up the home directory source
  file:
    path: "{{ item }}"
    state: absent
  loop:
    - "promtail-linux-{{ 'arm64' if ansible_architecture == 'aarch64' else 'amd64' }}.zip"
    - "promtail-linux-{{ 'arm64' if ansible_architecture == 'aarch64' else 'amd64' }}"


- name: Restart and enable the promtail service
  service:
    name: promtail
    daemon_reload: yes
    state: restarted
    enabled: yes